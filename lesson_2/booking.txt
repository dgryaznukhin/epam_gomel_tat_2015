

    [info] Playing test case 3-1. (add) Selenium Belavia Output
    [info] Executing: |open | /home/ | |
    [info] Executing: |click | id=tickets-1 | |
    [info] Executing: |click | css=#journey-type > label | |
    [info] Executing: |click | id=tickets-2 | |
    [info] Executing: |type | id=originLocation | GME |
    [info] Executing: |type | id=destinationLocation | KGD |
    [info] Executing: |type | id=departureDate | 2015-06-24 |
    [info] Executing: |type | id=returnDate | 2015-08-05 |
    [info] Executing: |type | id=add-in-1 | 2 |
    [info] Executing: |clickAndWait | css=div.btn-wdt > input[type="submit"] | |
    [info] Executing: |storeText | css=span.today | toDate |
    [info] Executing: |echo | ${toDate} | |
    [info] echo: ����� 24 ����
    [info] Executing: |storeText | css=div.departure > strong | toTimeDept |
    [info] Executing: |echo | ${toTimeDept} | |
    [info] echo: 09:45
    [info] Executing: |storeText | css=div.arrival > strong | toTimeArr |
    [info] Executing: |echo | ${toTimeArr} | |
    [info] echo: 10:10
    [info] Executing: |click | css=div.er.fare > label | |
    [info] Executing: |click | css=div.sf.fare > label | |
    [info] Executing: |storeText | //div[@id='outbound']/div[3]/div/div[2]/div[4]/label | toPriceSemi |
    [info] Executing: |click | css=div.ef.fare > label | |
    [info] Executing: |storeText | //div[@id='outbound']/div[3]/div/div[2]/div[5]/label | toPriceFlex |
    [info] Executing: |echo | ${toPriceFlex} | |
    [info] echo: 150,00 EUR
    [info] Executing: |storeText | css=#inbound > div.hdr > div.dates > span.today | fromDate |
    [info] Executing: |echo | ${fromDate} | |
    [info] echo: ����� 05 �������
    [info] Executing: |storeText | css=div.t-body > div.t-body > div.t-row > div.flight-avail > div.departure > strong | fromTimeDept |
    [info] Executing: |echo | ${fromTimeDept} | |
    [info] echo: 10:40
    [info] Executing: |storeText | css=div.t-body > div.t-body > div.t-row > div.flight-avail > div.arrival > strong | fromTimeArr |
    [info] Executing: |echo | ${fromTimeArr} | |
    [info] echo: 13:05
    [info] Executing: |click | css=div.t-body > div.t-body > div.t-row > div.fare-avail.ui-corner-all > div.er.fare > label | |
    [info] Executing: |storeText | //div[@id='inbound']/div[3]/div/div/div[2]/div[3]/label | fromPriceRestr |
    [info] Executing: |echo | ${fromPriceRestr} | |
    [info] echo: 70,00 EUR
    [info] Executing: |click | css=div.t-body > div.t-body > div.t-row > div.fare-avail.ui-corner-all > div.sf.fare > label | |
    [info] Executing: |storeText | //div[@id='inbound']/div[3]/div/div/div[2]/div[4]/label | fromPriceSemi |
    [info] Executing: |echo | ${fromPriceSemi} | |
    [info] echo: 80,00 EUR
    [info] Executing: |click | css=div.t-body > div.t-body > div.t-row > div.fare-avail.ui-corner-all > div.ef.fare > label | |
    [info] Executing: |storeText | //div[@id='inbound']/div[3]/div/div/div[2]/div[5]/label | fromPriceFlex |
    [info] Executing: |echo | ${fromPriceFlex} | |
    [info] echo: 150,00 EUR
    [info] Test case passed
    [info] Test suite completed: 1 played, all passed! 

