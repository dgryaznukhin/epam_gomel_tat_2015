package com.epam.gomel.tat.lesson_6.reporting;

public class Logger {
    // TODO wrap logger

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);


    public static void error(String s) {
        log.error(s);
    }

    public static void error(String s, Throwable e) {
        log.error(s, e);
    }
}
