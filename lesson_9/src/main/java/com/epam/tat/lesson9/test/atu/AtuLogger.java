package com.epam.tat.lesson9.test.atu;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.epam.tat.lesson9.utils.Logger;

/**
 * Created by Aleh_Vasilyeu on 4/1/2015.
 */
public class AtuLogger {

    public static void info(String message) {
        info(message, false);
    }

    public static void info(String message, boolean needScreenshot) {
        Logger.info(message);
        ATUReports.add(message, LogAs.INFO, needScreenshot ? new CaptureScreen(
                CaptureScreen.ScreenshotOf.BROWSER_PAGE) : null);
    }
}
