package com.epam.tat.lesson9.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @author Aleh_Vasilyeu
 */
public class FileTools {

    private static final Logger LOGGER = Logger.getLogger(FileTools.class);

    private static final String FILE_TYPE = "FILE";

    public static String getFileType(File file) {
        return getFileType(file.getName());
    }

    public static String getFileType(String filename) {
        String[] parts = filename.split("\\.");
        if (parts.length > 1) {
            return parts[parts.length - 1].toUpperCase();
        } else {
            return FILE_TYPE;
        }
    }

    /**
     * Gets canonical path to resource file.
     *
     * @param resourceFileLocalPath - resource file stored in 'resources' folder.
     * @return canonical path to resource file.
     */
    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileTools.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;
    }
}
